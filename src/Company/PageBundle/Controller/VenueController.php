<?php

namespace Company\Bundle\Web\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VenueController
 * @package Company\Bundle\Web\PageBundle\Controller
 */
class VenueController extends Controller
{

    /**
     * @param Request $request
     * @param $id
     * @param $locale
     *
     * @return array
     *
     * @Route("/{_locale}/venues/{id}", name="static_venue")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $id, $_locale)
    {
        $queryEntity = array(
            'site' => $this->get('company.site_settings')['search_site_tag'],
            'entityId' => $id,
            'entityType' => 'venues',
            'preview' => $request->get('preview', false),
            'locale' => $_locale
        );

        $retrievedVenue = $this->get('company.static_pages.response_adaptor')->getEntity($queryEntity);

        return array(
            'listing'        => $retrievedVenue,
            'omniture'       => $this->get('company.omniture.factory')->create('venue', $retrievedVenue->toArray()),
            'lf_seo_content' => $this->get('livefyre.client')->getSeoFragment($retrievedVenue->getMetadata()->getArticleId()),
            'locale'         => $_locale,
            'nearby'         => $this->get('company.search.nearby')->nearby($retrievedVenue),
        );
    }
}
