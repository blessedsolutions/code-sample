@venue
Feature: Venue homepage
  In order for the homepage to have all information a user needs, the following page content and functionality
  be present/working as expected

  Background:
    Given the following routes exist:
      | redirect_id  | slug                     | target              | supplier      | routingApplication    | isPublished |
      | NULL         | /london/cinemas/bfi-imax | /en_GB/venues/1234  | slug-listener | all                   | 1           |

  @javascript
  Scenario Outline: Venue page key attribute visibility for different viewports
    Given the viewport is "<viewport>"
      And I am on "london/cinemas/bfi-imax"
     Then I should see "BFI IMAX" in the ".listing_page_title" element
      And I should <see-or-not> "Cinemas" in the ".page_tag" element
      And I should see "Southbank" in the ".listings_flag" element
      And I should see "https://www.facebook.com/bfiimax" in the ".listing_details" element
      And I should see "Details" in the ".tab__button--current.tab__button--<orientation> .tab__title" element
  Examples:
    | viewport | see-or-not | orientation |
    | LG       | see        | horizontal  |
    | MD       | see        | horizontal  |
    | SM       | see        | vertical    |
    | XS       | see        | vertical    |

