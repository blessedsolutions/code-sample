<?php
namespace Company\BDD\Features\bootstrap;

use Behat\Gherkin\Node\TableNode;
use Company\Behat\BaseFeatureContext;
use Sanpi\Behatch\Context\BehatchContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Company\Component\Routing\Route;
use Behat\Behat\Event\BaseScenarioEvent;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Behat\Event\SuiteEvent;

//
// Require 3rd-party libraries here:
//
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

/**
 * Features context.
 */
class FeatureContext extends BaseFeatureContext implements KernelAwareInterface
{
    private $kernel;
    private $parameters = array();
    private $viewports = array("LG" => 1200, "MD" => 992, "SM" => 768, "XS" => 640);

    private static $instance = null;
    /*
     * Value returned from ScenarioEvent::getResult() when a scenario passed
     */
    const SCENARIO_EVENT_PASSED = 0;
    const DEFAULT_WINDOW_DIMENSIONS_WIDTH = 1200;
    const DEFAULT_WINDOW_DIMENSIONS_HEIGHT = 768;
    const DEFAULT_WAIT_TIME_FOR_ELEMENT = 10;

    use Traits\Zone;
    use Traits\Advert;
    use Traits\Widget;
    use Traits\Search;
    use Traits\Package;
    use Traits\Navigate;
    use Traits\Booking;
    use Traits\Identity;
    use Traits\Tab;
    use Traits\Film;
    use Traits\Blog;

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
        // Initialize your context here
        $this->useContext('behatch', new BehatchContext($parameters));
    }

    /**
     * Sets HttpKernel instance.
     *
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        self::$instance = clone $this;
    }

    /**
     * Gets the HttpKernel instance.
     *
     * @return KernelInterface
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * Gets the Container instance.
     *
     * @return Container
     */
    public function getContainer()
    {
        return $this->getKernel()->getContainer();
    }

    /**
     * @return Company\Component\Routing\Storage\PDOStorage service
     */
    protected function getRoutingDatabaseService()
    {
        return $this->getContainer()->get('routing.pdo_storage');
    }

    /**
     * @return Company\Component\Routing\Storage\MemcacheStorage service
     */
    protected function getRoutingCacheService()
    {
        return $this->getContainer()->get('routing.slug_storage.memcache');
    }

    /**
     * @BeforeSuite
     * Use to truncate the necessary tables
     * Behat insists Before Suite functions are static! So have you clone a copy of the class into the $instance var
     */
    public static function cleanDb(SuiteEvent $event)
    {
        self::$instance->truncateTable('routings');
    }

    /**
     * Truncates the specified table.
     *
     * @param string $tableName
     */
    protected function truncateTable($tableName)
    {
        if ($tableName == 'routings') {
            $routingDbService = $this->getRoutingDatabaseService();
            $routingDbService->deleteAll();
        }
    }

    /**
     * @BeforeScenario
     * Invalidates the keys stored in memcached used by the specified caching services
     */
    public function flushCache()
    {
        $this->getRoutingCacheService()->getMemcache()->flush();
    }

    /**
     * @BeforeSuite
     * Delete previous screenshots
     */
    public static function deleteScreenshots(SuiteEvent $event)
    {
        $files = array_merge(
            glob('src/Company/BDD/screenshots/Default/passed/*'),
            glob('src/Company/BDD/screenshots/Default/failed/*'),
            glob('src/Company/BDD/screenshots/OSXChrome/passed/*'),
            glob('src/Company/BDD/screenshots/OSXChrome/failed/*'),
            glob('src/Company/BDD/screenshots/OSXFirefox/passed/*'),
            glob('src/Company/BDD/screenshots/OSXFirefox/failed/*'),
            glob('src/Company/BDD/screenshots/OSXSafari/passed/*'),
            glob('src/Company/BDD/screenshots/OSXSafari/failed/*'),
            glob('src/Company/BDD/screenshots/Win7Chrome/passed/*'),
            glob('src/Company/BDD/screenshots/Win7Chrome/failed/*'),
            glob('src/Company/BDD/screenshots/Win7IE9/passed/*'),
            glob('src/Company/BDD/screenshots/Win7IE9/failed/*')
        );

        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }
    /**
     * @AfterScenario
     * Takes screenshots after every passed scenario
     */
    public function autoScreenshot($event)
    {
        try {
            if (($event instanceof BaseScenarioEvent) &&
                ($event->getResult() == self::SCENARIO_EVENT_PASSED) &&
                ($this->parameters['auto_screenshot'])) {
                $this->takeAScreenshot();
            }
        } catch (UnsupportedDriverActionException $e) {
            echo "\n FAILED TO TAKE SCREENSHOT FOR THIS SCENARIO: ".$e->getMessage()."\n\n";
        }
    }

    /**
     * Use to insert the urls used in your test into the routing db table
     *
     * @Given /^the following routes exist:$/
     */
    public function theFollowingRoutesExist(TableNode $table)
    {
        $routingDbService = $this->getRoutingDatabaseService();
        foreach ($table->getHash() as $row) {
            $route = Route::createRoute($row['slug'], $row['target'], $row['supplier'], $row['routingApplication'], $row['isPublished']);
            $routingDbService->updateRoute($route);
        }
    }

    /**
     * @BeforeScenario
     * Reset window to default dimensions for supported drivers.
     */
    public function resetWindowToDefaultDimensions()
    {
        try {
            $this->setWindowDimensions(self::DEFAULT_WINDOW_DIMENSIONS_WIDTH, self::DEFAULT_WINDOW_DIMENSIONS_HEIGHT);
        } catch (UnsupportedDriverActionException $e) {
            /*
             * No error message to be outputted.
             * Default driver is headless and does not support window resizing.
             * Exceptions will be thrown elsewhere for drivers that do support window resizing that fail
             */
        }
    }

    /**
     * Use to set the window dimensions for the test. Useful when testing responsive applications
     *
     * @Given /^The window dimensions are ([a-z0-9\-_\.]+) by ([a-z0-9\-_\.]+)$/
     */
    public function setWindowDimensions($width, $height)
    {
        $this->getSession()->getDriver()->resizeWindow((int)$width, (int)$height,'current');
    }

    /**
     * @Given /^the viewport is "([^"]*)"$/
     */
    public function setViewport($viewport)
    {
        $this->setWindowDimensions($this->viewports[$viewport] + 16, self::DEFAULT_WINDOW_DIMENSIONS_HEIGHT);
    }

    /**
     * Capture a screenshot and specify its name. It will be stored in src/Company/BDD/screenshots/passed/
     *
     * @Then /^take a screenshot named "([^"]*)"$/
     */
    public function takeAScreenshot($filename=null)
    {
        $imageName = !empty($filename) ? trim($filename) : $this->getScreenshotFilename();
        if (!self::endsWith($imageName, ".png")) $imageName .= ".png";
        $imageData = $this->getSession()->getDriver()->getScreenshot();
        file_put_contents($this->parameters['auto_screenshot_dir'].$imageName, $imageData);
    }

    /**
     * Generates a unique filename for a screenshot
     * @return string
     */
    protected function getScreenshotFilename()
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '-', $this->getSession()->getCurrentUrl()).'-'.uniqid().'.png';
    }

    /**
     * @param $lambda
     * @param int $wait
     * @return bool
     * @throws \Exception
     * A function designed to wait until the logic executed in the anonymous function stored in $lambda returns true
     * within the timeout number of seconds defined in $wait
     * Use this function when waiting for elements to appear in the page that are loaded via ajax.
     * For example 3rd party adverts
     * See http://docs.behat.org/en/v2.5/cookbook/using_spin_functions.html for overview.
     */
    public function spin ($lambda, $wait = 60)
    {
        for ($i = 0; $i < $wait; $i++)
        {
            try {
                if ($return = $lambda($this)) {
                    return $return;
                }
            } catch (\Exception $e) {
                // do nothing
            }

            \usleep(500000);
        }

        $backtrace = debug_backtrace();

        throw new \Exception(
            "Timeout thrown by " . $backtrace[0]['class'] . "::" . $backtrace[0]['function'] . "()\n" .
            $backtrace[0]['file'] . ", line " . $backtrace[0]['line']
        );
    }

    /**
     * @When /^I click the "([^"]*)" element$/
     */
    public function iClickTheElement($selector)
    {
        $element = $this->getSession()->getPage()->find('css', $selector);
        assertTrue(isset($element), 'Could not find element ' . $selector);
        $element->click();
    }

    /**
     * @When /^I click the "([^"]*)" button$/
     */
    public function iClickTheButton($button)
    {
        $this->iClickTheElement('button' . $button);
    }

    /**
     * @When /^I select "([^"]*)" from "([^"]*)" dropdown$/
     */
    public function iSelectFromDropdown($option, $select)
    {
        $listElement = $this->getSession()->getPage()->find('css', 'select' . $select);
        assertTrue(isset($listElement), 'Could not find list element: ' . $select);
        $listElement->selectOption($option);
    }

    
    /**
     * @Then /^the query parameter "([^"]*)" should be "([^"]*)"$/
     */
    public function theQueryParameterShouldBe($key, $value)
    {
        $queryParams = $this->getQueryStringParamsFromUrl($this->getSession()->getCurrentUrl());
        if (!isset($queryParams[$key])) {
            throw new \Exception('Key: '.$key.' was not found in the query string');
        }
        if ($queryParams[$key] != trim($value)) {
            throw new \Exception($key.' parameter in the query string did not contain '.$value.' -  '.$queryParams[$key]. ' was found instead.');
        }
    }

    /**
     * Return the parameters in the query string of a URL as a key value array.
     * @param $url
     * @return array
     */
    public function getQueryStringParamsFromUrl($url)
    {
        $params = [];
        $urlExploded = explode('?', $url);
        if (isset($urlExploded[1])) {
            $queryDataPairs = explode('&', $urlExploded[1]);
            foreach ($queryDataPairs as $pair) {
                $keyVal = explode('=', $pair);
                $params[$keyVal[0]] = $keyVal[1];

            }
        }
        return $params;
    }

    /**
     * Return the element on a page for a given CSS selector using the Spin function to wait for it
     *
     * @param $needle
     * @param $haystack
     * @param $selector
     * @return string
     */
    public function getPageElementWithSpin(&$needle, $haystack, $selector)
    {
        $this->spin(function () use (&$needle, $haystack, $selector) {
            $needle = $haystack->find('css', $selector);
            return (is_object($needle)) ? true : false;
        }, self::DEFAULT_WAIT_TIME_FOR_ELEMENT);
    }

    /**
     * Convert any accented characters in a string to plain text
     *
     * @param $text
     * @return string
     */
    public function convertAccentCharsToPlain($text)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $text);
    }

    /**
     * @Then /^I should find a meta tag with attribute "([^"]*)" equals "([^"]*)" that contains "([^"]*)"$/
     */
    public function iShouldFindAMetaTagWithPropertyAndContent($attributeName, $property, $content)
    {
        $page = $this->getSession()->getPage();
        $metaTags = $page->findAll('css', 'meta');

        foreach ($metaTags as $tag) {
            if ($tag->getAttribute($attributeName) == $property ) {

               $found = false !== \strpos($tag->getAttribute('content'), $content);
               assertTrue($found, $content . ' can not be found in : ' . $tag->getAttribute('content') );
               if ($found) {
                   return;
               }
           }
        }

        throw new \Exception("Unable to find a meta tag with attribute: " . $attributeName ." that contains: " . $property);
    }

}
