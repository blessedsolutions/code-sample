Code snippets from a deployable and testable Symfony 2 application.
Snippets include sample:

- Controller

- Twig template

- Guzzle service json

- Phing build file for use in Jenkins and Vagrant

- Behat configuration for local environment and Continuous Integration environment

- Ansible playbook to provision a local VM via Vagrant